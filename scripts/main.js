window.onload = function(){

    //_Sprite.graphic.html.addEventListener("touchstart", self.OnTouch, false);
    //_Sprite.graphic.html.addEventListener("mousedown", self.OnTouch, false);
    // remove touch listener
    //_Sprite.graphic.html.removeEventListener("touchstart", self.OnTouch);
    //_Sprite.graphic.html.removeEventListener("mousedown", self.OnTouch);
   
}

/**
 *
 *
 * Vec2
 *
 * Defines a 2D vector math to be performed on arrays
 *
 * 
 * */

var vec2 = {};

//function vec2(x, y) {
//    this[0] = x ? x : 0;
//    this[1] = y ? y : 0;
//}

vec2.create = function(vec){
    //Using a matrix array
    var dest = new Array(2);
    
    if (vec) {
        dest[0] = vec[0];
        dest[1] = vec[1];
    }
    else{
        dest[0] = 0;
        dest[1] = 0;
    }
    
    return dest;
}

/**
 * vec2.set
 * Set destination vec2 to vec
 **/
vec2.set = function(dest, vec){
    
    dest[0] = vec[0];
    dest[1] = vec[1];
    
    return dest;
};

/**
 * vec2.add
 * Perform vector addition
 **/
vec2.add = function(a, b, dest){
    //If dest vector
    if(!dest || a == dest){
        a[0] += b[0];
        a[1] += b[1];
        return a;
    }
    
    dest[0] = a[0] + b[0];
    dest[1] = a[1] + b[1];
    return dest;
};

/**
 * vec2.subtract
 * Perform vector subtraction
 **/
vec2.subtract = function(a, b, dest){
    //If dest vector
    if (!dest || a == dest) {
        a[0] -= b[0];
        a[1] -= b[1];
        
        return a;
    }
    
    dest[0] = a[0] - b[0];
    dest[1] = a[1] - b[1];
    return dest;
};

/**
 * vec2.negate
 * Negate the values of the vector
 **/
vec2.negate = function(vec, dest){
    //If there is no dest vector, set dest == vec
    if (!dest) {
        dest = vec;
    }
    
    dest[0] = -vec[0];
    dest[1] = -vec[1];
    return dest;
};

/**
 * vec2.scale
 * Multiply the vector by a scalar
 **/
vec2.scale = function(vec, scalar, dest){
    if (!dest || vec == dest) {
        vec[0] *= scalar;
        vec[1] *= scalar;
        return vec;
    }
    
    dest[0] = vec[0] * scalar;
    dest[1] = vec[1] * scalar;
    return dest;
};

/**
 * vec2.normalize
 * Normalize the vector, return 0 vector if length is 0
 **/
vec2.normalize = function(vec, dest){
    if (!dest || dest == vec) {
        dest = vec;
    }
    
    //Create temp values
    var x = vec[0];
    var y = vec[1];
    
    //Get magnitude
    var mag = Math.sqrt(x*x + y*y);
    
    if (!mag) {
        dest[0] = 0;
        dest[1] = 0;
    } else if (mag == 1) {
        //Vector is already normalized
        dest[0] = x;
        dest[1] = y;
        return dest;
    }
    
    mag = 1 / mag;      //Get inverse magnitude
    dest[0] = x*mag;    //Multiply each value by magnitude
    dest[1] = y*mag;
    return dest;
};

/**
 * vec2.perp
 * Generate a perpindicular vector
 **/
vec2.perp = function(vec, dest){
    if (!dest || dest == vec) {
        var x = vec[0];
        var y = vec[1];
        
        vec[0] = -y;
        vec[1] = x;
        return vec;
    }
    
    dest[0] = -vec[1];
    dest[1] = vec[0];
    return dest;
};

/**
 * vec2.magnitude
 * Calculates length of a vec2
 **/
vec2.magnitude = function(vec){
    var x = vec[0];
    var y = vec[1];
    
    return Math.sqrt(x*x + y*y);
};

/**
 * vec2.magnitudeSq
 * Calculates length squared of a vec2
 **/
vec2.magnitudeSq = function(vec){
    var x = vec[0];
    var y = vec[1];

    return x * x + y * y;
};

/**
 * vec2.dot
 * Calculates the dot product of two vec2's
 **/
vec2.dot = function(a, b){
    return a[0] * b[0] + a[1] * b[1];
};

/**
 * vec2.direction
 * Generates a normalized vector pointing from a to b
 **/
vec2.direction = function(a, b, dest){
    if (!dest) {
        dest = a;
    }
    
    var x = a[0] - b[0];
    var y = a[1] - b[1];
    
    var mag = Math.sqrt(x*x + y*y);
    
    if (!mag){
        dest[0] = 0;
        dest[1] = 0;
        return dest;
    }
    
    mag = 1 / mag;
    dest[0] = x * mag;
    dest[1] = y * mag;
    return dest;
};

/**
 * vec2.string
 * Return a string representation of a vector
 **/
vec2.string = function(vec){
    return '[' + vec[0] + ',' + vec[1] + ']';
};

/**
 *
 *
 *Physics core
 *
 *Defines an object which holds all physics objects currently active in the system
 *
 *
 */

var Physics = {
    
    _objects : [],
    
    //Members used for collision information
    _colNormal : null,
    _colDepth : null,
    _collision : new CCollision(),
    
    //Forces
    _gravity : vec2.create(),
    _force : vec2.create(),
    
    //Public methods
    Update : function(dt){
        //For each circle
        for (i = Game.balls.length - 1; i >= 0; i--) {
            
            //Test for collision
            if (Physics.CollisionTest(Game.balls[i])) {
                //Resolve collision
                Physics.CollisionResolution(collision, Game.balls[i]);
            }
            
            //Accumulate forces
            Physics.AccumulateForce();
            
            //Integrate 
            Physics.Integrate(Game.balls[i].physics, dt);
        }
    },

    
    //Private methods
    
    /**
     *CollisionTest
     *Takes a PhysicsCircle object as a paramater
     *Tests the circle against all lines
     *If a collision occurs record collision normal and interpenetration
     *Return true or false
     */
    CollisionTest : function(objA){
        for (var i = Game.lines.length - 1; i >= 0; i--) {
            //Test distance from circle to line
            var distSq = objA.physics.LineDistanceSq(Game.lines[i]);
            if (distSq < objA.physics.radius * objA.physics.radius) {
                //A collision has occurred
                _colDepth = Math.sqrt((objA.physics.radius * objA.physics.radius) - distSq);
                
                //Get the closest point on the line
                var closestPoint = CircleLineIntersect(objA, Game.lines[i]);
                
                //Compute the normal as circle position - closest point
                _colNormal = vec2.subtract(objA.pos, closestPoint);
                vec2.normalize(_colNormal);
                
                return true;
            }
        }
        
        return false;
    },
    /**
     *CollisionResolution
     *Takes a physics object and collision object
     *Calculates and applies impulse to object velocity
     */
    CollisionResolution : function(collision, ObjA, ObjB){
        
        //Calculate the relative velocity
        var relVelocity = new vec2();
        
        if (ObjB) {
            relVelocity = ObjB.velocity - ObjA.velocity;
        }else{
            relVelocity = ObjA.velocity;
        }
        
        //Calculate relative velocity about collision normal
        var velNormal = vec2.dot(relVelocity, collision.normal);
        
        //Are the objects separating?
        if (velNormal > 0) {
            //Do nothing
            return;
        }
        
        //Calculate restitution
        var e;
        if (ObjB) {
            e = Math.min(ObjA.restitution, ObjB.restitution);
        }
        else{
            e = ObjA.restitution;
        }
        
        //Calculate impulse scalar
        var j = -(1 + e) * velNormal;
        
        if (ObjB) {
            j /= (1/ObjA.mass) + (1/ObjB.mass);
        }
        else{
            j /= 1/ObjA.mass;
        }
        
        //Apply impulse
        var impulse = vec2.scale(collision.normal, j);
        var appliedVel = vec2.scale(impulse, 1/impulse);
        ObjA.velocity = vec2.subtract(ObjA.velocity, appliedVel);
        if (ObjB) {
            ObjB.velocity = vec2.add(ObjB.velocity, appliedVel);
        }
    },
    /**
     *AccumulateForces
     *Accumulates all the active forces on an object
     */
    AccumulateForce : function(){
        vec2.set(this._force, [0, 0]);
        this._force += this._gravity;
    },
    /**
     *Integrate
     *Integrates and object using force
     *Takes a physics object as a parameter
     *Updates object acceleration and velocity
     *Moves the object
     */
    Integrate : function(obj, dt){
        obj.acceleration = this._force * (1 / obj.mass);
        obj.velocity += obj.acceleration * dt;
        obj.position += obj.velocity * dt;
        Game.balls[obj.iID - 1].Move(obj.physics.position);
    },
    
    //Protected methods
    SetGravity : function(g){
        Physics._gravity = g;
    },
    GetGravity : function(){
        return Physics._gravity;
    }
}

/**
 *Ball object
 */
var CBall = function(iID){
    this.id = iID;
    this.position = [];
    this.radius = 50;
    this.moving = false;
    this.obj = Game.psl.Create.HTML('div');
    this.obj.html.id = iID;
    this.physics = new CPhysicsCircle(iID, this.radius);
    
    //Private self reference
    var _self = this;
    
    
    //Append to HTML body
    this.obj.Append.ToHTML(document.body);
    this.obj.Append.Class("ball");
    

    this.obj.html.onmousedown = function(e){
        _self.moving = true;
    };
    this.obj.html.onmouseup = function(e){
        _self.moving = false;
    }
    this.obj.html.onmousemove = function(e){
        if (_self.moving) {
            _self.Move([e.x, e.y]);
        }
    }
    
    this.touchStart = function(e){
        this.moving = true;
    };
    this.touchMove = function(e){
        this.Move([e.x, e.y]);
    };
    this.touchEnd = function(e){
        this.moving = true;
    }
}
CBall.prototype.Move = function(pos){
    this.position[0] = pos[0];
    this.position[1] = pos[1];
    this.physics.position[0] = pos[0];
    this.physics.position[1] = pos[1];
    this.obj.html.style.top = (pos[1] - 30) + "px";
    this.obj.html.style.left = (pos[0] - 30) + "px";
}
CBall.prototype.IntersectPoint = function(point){
    var dist = new vec2();
    dist[0] = point[0] - this.position[0];
    dist[1] = point[1] - this.position[1];
    var mag = vec2.magnitude(dist);
    if (mag < this.radius) {
        return true;
    }
    else return false; 
}

/**
 *Line object
 */
var CLine = function(iID){
    this.start = [0,0];
    this.end = [0,0];
}
CLine.prototype.Draw = function(){
    Game.ctx.moveTo(this.start[0], this.start[1]);
    Game.ctx.lineTo(this.end[0], this.end[1]);
    Game.ctx.stroke();
}

/**
 *Rect object
 */
var CRect = function(iID){
    this.top = 0;
    this.left = 0;
    this.width = 50;
    this.height = 200;
    
}
CRect.prototype.Draw = function(){
    Game.ctx.fillRect(this.top, this.left, this.width, this.height);
}


/**
 *
 *
 * Physics Object
 * Defines a base 2D physics object
 * 
 *
 **/

function CPhysicsObject(id, position) {
    
    //Private variables
    var _position = new vec2();
    var _velocity = new vec2();
    var _angularVelocity = new vec2();
    var _acceleration = new vec2();
    var _angularAcceleration = new vec2();
    var _mass = 0;
    var _staticFriction =  0;
    var _restitution = 0;
    var _id;
    
    //Public variables
    this.isActive = false;
    
    _id = id;
    _position = pos ? pos : [0,0];
    
    //Private methods
    function setID(id) {
        _ID = id;
    }
    
    //Priveleged methods
    
    /*
     *Setters
     */
    this.setPosition = function(pos){
        _position.x = pos[0];
        _position.y = pos[1];
    };
    
    this.setVelocity = function(vel){
        _velocity.x = vel[0];
        _velocity.y = vel[1];
    };
    
    this.setAngularVelocity = function(ang){
        _angularVelocity.x = ang[0];
        _angularVelocity.y = ang[1];
    }
    
    this.setAcceleration = function(accel){
        _acceleration.x = accel[0];
        _acceleration.y = accel[1];
    };
    
    this.setMass = function(mass){
        _mass = mass;
    };
    
    this.setStaticFriction = function(f){
        _staticFriction = f;
    };
    
    this.setRestitution = function(f){
        _restitution = f;
    };
    
    /*
     *Getters
     */
    this.getPosition = function(){
        return this._position;
    };
    
    this.getVelocity = function(){
        return this._velocity;
    };
    
    this.getAngularVelocity = function(){
        return this._angularVelocity;
    }
    
    this.getAcceleration = function(){
        return this._acceleration;
    };
    
    this.getMass = function(mass){
        return this._mass;
    };
    
    this.getStaticFriction = function(){
        return this._staticFriction;
    };
    
    this.getRestitution = function(){
        return this._restitution;
    }
    
    this.getID = function(){
        return this._id;
    }
}


/**
 *Physics Circle
 *Defines a 2D physics circle <ALERT> Should inherit from CPhysicsObject
 */
var CPhysicsCircle = function(id, radius, pos){

    //Public variables
    this.position = vec2.create(0, 0);
    this.velocity = vec2.create(0, 0);
    this.angularVelocity = vec2.create(0, 0);
    this.acceleration = vec2.create(0, 0);
    this.angularAcceleration = vec2.create(0, 0);
    this.radius = 0;
    this.mass = 0;
    this.staticFriction = 0;
    this.restitution = .9;
    this.iID = id;
    
    this.radius = radius ? radius : 50;
}

/**
 *Circle-Line intersect test
 *Returns smallest distance to line squared
 */
CPhysicsCircle.prototype.LineDistanceSq = function(line){
    //Compute the line
    var ab = [line.end[0] - line.start[0], line.end[1] - line.start[1]];
    //Compute start to center
    var ac = [this.position[0] - line.start[0], this.position[1] - line.start[1]];
    //Compute end to center
    var bc = [this.position[0] - line.end[0], this.position[1] - line.end[1]];
    
    var e = vec2.dot(ac, ab);
    
    //If projected outside of ab
    if (e <= 0) {
        e = vec2.dot(ac, ac);
        return e;
    }
    
    //If projected inside line
    var f = vec2.dot(ab, ab);
    if (e >= f) {
        return vec2.dot(bc, bc);
    }
    
    //If projected outside of ab on the other side
    return vec2.dot(ac, ac) - e * e / f;
}

/**
 *Circle-Line intersect test
 *Takes a circle and a line, returns the closest point
 */
var CircleLineIntersect = function(circle, line){
    //Compute the line
    var ab = vec2.create();
    vec2.subtract(line.end, line.start, ab);
    
    var toStart = [];
    vec2.subtract(circle.position, line.start, toStart);
    
    var num = vec2.dot(toStart, ab);
    var den = vec2.dot(ab, ab);
    
    var t = num / den;
    
    //If outside line clamp points to end
    if (t < 0) {
        t = 0;
    }
    if (t > 1) {
        t = 1;
    }
    var ret = [line.start[0] + (t * ab[0]), line.start[1] + (t * ab[1])];
    return ret;
}

/**
 *
 *
 * Collision Object
 * Defines an object which contains a collision normal and penetration depth
 *
 */
function CCollision(normal, depth) {
    this.normal = normal ? normal : [0, 0];
    this.depth = depth ? depth : 0;
}

var Physics = {
    
    _objects : [],
    
    //Members used for collision information
    _colNormal : [],
    _colDepth : null,
    _collision : new CCollision(),
    
    //Forces
    _gravity : vec2.create([0, 9.8]),
    _force : vec2.create([0, 0]),
    _tempAccel : vec2.create([0, 0]),
    _tempVel : vec2.create([0, 0]),
    _linearProject : vec2.create([0, 0]),
    
    //Public methods
    Update : function(dt){
        
        //Zero linear projection
        Physics._linearProject[0] = 0;
        Physics._linearProject[1] = 0;
        
        //For each circle
        for (i = Game.balls.length - 1; i >= 0; i--) {
            
            //Test for collision
            if (Physics.CollisionTest(Game.balls[i])) {
                //Resolve collision
                Physics.CollisionResolution(Physics._collision, Game.balls[i].physics);
            }
            
            //Accumulate forces
            Physics.AccumulateForce();
            
            //Integrate 
            Physics.Integrate(Game.balls[i].physics, dt);
        }
    },

    
    //Private methods
    
    /**
     *CollisionTest
     *Takes a PhysicsCircle object as a paramater
     *Tests the circle against all lines
     *If a collision occurs record collision normal and interpenetration
     *Return true or false
     */
    CollisionTest : function(objA){
        for (var i = Game.lines.length - 1; i >= 0; i--) {
            //Test distance from circle to line
            var distSq = objA.physics.LineDistanceSq(Game.lines[i]);
            if (distSq < objA.physics.radius * objA.physics.radius) {
                //A collision has occurred
                Physics._collision.depth = Math.sqrt((objA.physics.radius * objA.physics.radius) - distSq);
                
                //Get the closest point on the line
                var closestPoint = CircleLineIntersect(objA.physics, Game.lines[i]);
                
                //Compute the normal as circle position - closest point
                vec2.subtract(objA.physics.position, closestPoint, Physics._collision.normal);
                vec2.normalize(Physics._collision.normal);
                
                return true;
            }
        }
        
        return false;
    },
    /**
     *CollisionResolution
     *Takes a physics object and collision object
     *Calculates and applies impulse to object velocity
     */
    CollisionResolution : function(collision, ObjA, ObjB){
        
        //Calculate the relative velocity
        var relVelocity = [];
        
        if (ObjB) {
            vec2.subtract(ObjB.velocity, ObjA.velocity, relVelocity); //relVelocity = ObjB.velocity - ObjA.velocity;
        }else{
            relVelocity = ObjA.velocity;
        }
        
        //Calculate relative velocity about collision normal
        var velNormal = vec2.dot(relVelocity, collision.normal);
        
        //Are the objects separating?
        if (velNormal > 0) {
            //Do nothing
            return;
        }
        
        //Calculate restitution
        var e;
        if (ObjB) {
            e = Math.min(ObjA.restitution, ObjB.restitution);
        }
        else{
            e = ObjA.restitution;
        }
        
        //Calculate impulse scalar
        var j = -(1 + e) * velNormal;
        
        if (ObjB) {
            j /= (1/ObjA.mass) + (1/ObjB.mass);
        }
        else{
            j /= 1/ObjA.mass;
        }
        
        //Apply impulse
        var impulse = [];
        vec2.scale(collision.normal, j, impulse);
        var appliedVel = [];
        vec2.scale(impulse, 1/ObjA.mass, appliedVel);
        vec2.add(ObjA.velocity, appliedVel);
        if (ObjB) {
            ObjB.velocity = vec2.add(ObjB.velocity, appliedVel);
        }
        
        //Set linear projection
        var correct = collision.depth - 0.01;
        correct = Math.max(correct, 0.0);
        correct /= ObjA.mass * 0.2;
        vec2.scale(collision.normal, correct, impulse);
        vec2.scale(impulse, 1 / ObjA.mass, Physics._linearProject);
    },
    /**
     *AccumulateForces
     *Accumulates all the active forces on an object
     */
    AccumulateForce : function(){
        this._force[0] = 0;
        this._force[1] = 0;
        this._force[0] += this._gravity[0];
        this._force[1] += this._gravity[1];
        //this._force = vec2.subtract(this._force, this._force);
        ////this._force += vec2.add(this._force, this._gravity);
        //this._force = this._force;
    },
    /**
     *Integrate
     *Integrates and object using force
     *Takes a physics object as a parameter
     *Updates object acceleration and velocity
     *Moves the object
     */
    Integrate : function(obj, dt){
        vec2.scale(this._force, (1 / obj.mass), obj.acceleration);
        vec2.scale(obj.acceleration, dt, Physics._tempAccel);
        
        vec2.add(obj.velocity, Physics._tempAccel, obj.velocity);
        vec2.scale(obj.velocity, dt, Physics._tempVel);
        
        //vec2.add(obj.position, Physics._linearProject, obj.position);
        vec2.add(obj.position, Physics._tempVel, obj.position);
        
        Game.balls[obj.iID - 1].Move(obj.position);
    },
    
    //Protected methods
    SetGravity : function(g){
        Physics._gravity[1] = g;
    },
    GetGravity : function(){
        return Physics._gravity[1];
    }
}

/**
 *Game
 */
Game ={
    canvas: null,
    ctx: null,
    psl: null,
    sound: null,
    balls: [],
    lines: [],
    rects: [],
    gameActive: false, 
    activeID: -1,
    lineID: 0,
    editing: false,
    moving: false,
    SetColor: function(color){
        Game.ctx.strokeStyle = color;
        Game.ctx.fillStyle = color;
    },
    Clear: function(){
        Game.ctx.clearRect(0,0,1024,768);
    },
    AddLineEvents: function(){
        Game.canvas.addEventListener('mousedown',Game.Events.touchstart, false);
        Game.canvas.addEventListener('mouseup',Game.Events.touchend, false);
        Game.canvas.addEventListener('mousemove',Game.Events.touchmove, false);
        Game.gameActive = false;
    },
    RemoveLineEvents: function(){
        Game.canvas.removeEventListener('mousedown',Game.Events.touchstart, false);
        Game.canvas.removeEventListener('mouseup',Game.Events.touchend, false);
        Game.canvas.removeEventListener('mousemove',Game.Events.touchmove, false);
        Game.gameActive = true;
    },
    Events: {
        touchstart: function(e){
            Game.editing = true;
            var line = new CLine(Game.lineID);
            Game.lineID++;
            line.start[0] = e.x;
            line.start[1] = e.y;
            line.end[0] = e.x;
            line.end[1] = e.y;
            Game.lines.push(line);
        },
        touchend: function(e){
            var line = Game.lines[Game.lines.length - 1];
            line.end[0] = e.x;
            line.end[1] = e.y;
            Game.Draw();
            Game.editing = false;
        },
        touchmove: function(e){
            if(Game.editing){
                var line = Game.lines[Game.lines.length - 1];
                line.end[0] = e.x;
                line.end[1] = e.y;
                //Game.Draw();
            }
        }
    },
    Draw: function(){
        Game.Clear();
        for(var i = Game.lines.length - 1; i >= 0; i--){
            Game.lines[i].Draw();
        }
        for(var i = Game.rects.length - 1; i >= 0; i--){
            Game.rects[i].Draw();
        }
    },
    Update: function(dt){
        //....Put your Physics Update call here....
        if (!Game.editing) {
            Physics.Update(dt);
        }
        
        
    },
    CreateBall: function(id, pos){
        var ball = new CBall(id);
        ball.Move(pos);
        ball.physics.mass = 1;
        Game.balls.push(ball);
    },
    Init: function(){
        //Load Playstyle Labs Module
        Game.psl = com.playstylelabs.Instance();
        
        //
        //  Load Canvas
        //
        Game.canvas = document.getElementById("background");
        Game.canvas.width = "1024";
        Game.canvas.height = "768";
        Game.ctx = Game.canvas.getContext('2d');
        
        //
        //  Load Sound
        //
        Game.sound = Game.psl.Audio.Add("touch", "sounds/touch", false, false);
        Game.sound.Play();
 
        Game.SetColor("red");
        
        //
        //  Create a ball
        //
        //var ball = new CBall(1);
        //ball.Move([130,130]);
        //Game.balls.push(ball);
        Game.CreateBall(1, [130, 130]);

        //Change color
        Game.SetColor("black");
        //
        //  Create Rect
        //
        var rect = new CRect(1);
        rect.top = 200;
        rect.left = 568;
        rect.Draw();
        Game.rects.push(rect);
        

        
        //
        //  Create canvas outline
        //
        var line = new CLine(Game.lineID);
        Game.lineID++;
        line.start[0] = 0;
        line.start[1] = 0;
        line.end[0] = 1024;
        line.end[1] = 0;
        line.Draw();
        Game.lines.push(line);
        
        var line = new CLine(Game.lineID);
        Game.lineID++;
        line.start[0] = 0;
        line.start[1] = 0;
        line.end[0] = 0;
        line.end[1] = 768;
        line.Draw();
        Game.lines.push(line);
        
        var line = new CLine(Game.lineID);
        Game.lineID++;
        line.start[0] = 1024;
        line.start[1] = 0;
        line.end[0] = 1024;
        line.end[1] = 768;
        line.Draw();
        Game.lines.push(line);
        
        var line = new CLine(Game.lineID);
        Game.lineID++;
        line.start[0] = 0;
        line.start[1] = 768;
        line.end[0] = 1024;
        line.end[1] = 768;
        line.Draw();
        Game.lines.push(line);
        
        //
        //  Set up touch (mouse) events
        //
        Game.AddLineEvents();
        
        
        //
        //  Set up Key Event Handlers
        //      To start/stop line drawing
        //
        Game.psl.Input.Register.Keyboard.Event.Keydown('A', Game.RemoveLineEvents);
        Game.psl.Input.Register.Keyboard.Event.Keydown('Z', Game.AddLineEvents);
        
        /**
         *Set up gravity
         */
        Physics.SetGravity(9.8);
        
        
        // main game update loop
        var lastUpdate = 0;
        var time;
        var dt;
        (Update = function(){
            time = Game.psl.Get.Time()
            
            dt = time - lastUpdate;
            
            if(dt > 80){
                lastUpdate = time;
                dt = 0;
            }
            
            dt *= .01;
                
            if (Game.gameActive) {
                // update game ents
                Game.Update(dt);
            }
            
                
                
            
            window.requestAnimFrame(Update);
            //window.webkitRequestAnimationFrame(Update);
        })();
    }
}

//http://phrogz.net/tmp/image_move_sprites_css.html