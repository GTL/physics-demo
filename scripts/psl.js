var com  = com ? com : {};

//Utility Functions, Classes and Objects
com.playstylelabs = (function(){
    //PRIVATE MODULE PROPERTIES
    var pInstance = null;
    
    //CONSTANTS
    var NAME = "PlayStyleLabs";
    var VERSION = "0.1";
    var PATH = "scripts/psl.js";
    var BROWSER = null;
    /**
    * Provides requestAnimationFrame in a cross browser way.
    */
   window.requestAnimFrame = (function() {
     return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.oRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function(/* function FrameRequestCallback */ callback, /* DOMElement Element */ element) {
              window.setTimeout(callback, 1000/60);
            };
   })();

    //Adds inheritance method to Function object
    /*
        MyChild = function(){ ... }
        MyChild.inheritsFrom(MyParent);
    */
    Function.prototype.inherits = function( parentClassOrObject ){ 
        if ( parentClassOrObject.constructor == Function ) 
        { 
                //Normal Inheritance 
                this.prototype = new parentClassOrObject;
                this.prototype.constructor = this;
                this.prototype.parent = parentClassOrObject.prototype;
        } 
        else 
        { 
                //Pure Virtual Inheritance 
                this.prototype = parentClassOrObject;
                this.prototype.constructor = this;
                this.prototype.parent = parentClassOrObject;
        } 
        return this;
    }
    //Adds a bind method to Function object, that will ensure
    //  scope of the "this" parameter is set appropriately
    /*
        myClass = function()
        {
            this.myFunc = function()
            {
                  ... do stuff ...
            }.bind(this);
          
        }
        
        myClass.prototype.secondFun = function()
        {
          
        }
        var testObj = new myClass();
        someObj.func = testObj.secondFun.bind(testObj);
    */
    Function.prototype.bind = function(scope){
        var _function = this;
        
        return function() {
          return _function.apply(scope, arguments);
        }
    }
                            
                            
    //INTERFACE TO BIOLUCID MODULE
    var constructor = function(){

        //PRIVATE OBJECTS, PROPERTIES AND CLASS DEFINITIONS
        //
        
        
        //Animation Classes
        var CSprite = function(sName){
            var self = this;
            this.name = sName;
            this.graphic = pInstance.Create.HTML("div");
            this.graphic.html.id = sName;
            this.graphic.html.style.position = "absolute";
            this.Position ={
                x: 0,
                y: 0,
                z: 0
            }
            this.Rotation ={
                x: 0,
                y: 0,
                z: 0
            }
            this.Move = {
                Up: function(iDist){
                    self.Position.y -= iDist;
                    self.graphic.html.style.top = (self.Position.y).toFixed(0) + "px";
                },
                Down: function(iDist){
                    self.Position.y += iDist;
                    self.graphic.html.style.top = (self.Position.y).toFixed(0) + "px";
                },
                Left: function(iDist){
                    self.Position.x -= iDist;
                    self.graphic.html.style.left = (self.Position.x).toFixed(0) + "px";
                },
                Right: function(iDist){
                    self.Position.x += iDist;
                    self.graphic.html.style.left = (self.Position.x).toFixed(0) + "px";
                },
                ToPosition: function(iX, iY){
                    self.Position.x = iX;
                    self.Position.y = iY;
                    
                    if(self.graphic.html.style.webkitTransform != undefined)
                        self.graphic.html.style.webkitTransform = "translate("+ (self.Position.x).toFixed(0) +"px,"+ (self.Position.y).toFixed(0) +"px)";
                    else
                        self.graphic.html.style.MozTransform = "translate("+ (self.Position.x).toFixed(0) +"px,"+ (self.Position.y).toFixed(0) +"px)";
                    //self.graphic.html.style.left = (self.Position.x).toFixed(0) + "px";
                    //self.graphic.html.style.top  = (self.Position.y).toFixed(0) + "px";
                }
            }
            
            this.Rotate = function(deg){
                self.graphic.html.style.webkitTransform = "rotate("+ deg +"deg)";
            }
            
            this.Spin = {
                X: function(deg){
                    
                },
                Y: function(deg){
                    
                }
            }
            
            
            this.Animation = {
                list: pInstance.Create.Map(),
                Current: {
                    animation: null,
                    frames: 0,
                    index: 0
                },
                loop: false,
                lastUpdate: 0,
                NextFrame: function(){//Transtion to next animation frame
                    self.Animation.lastUpdate = pInstance.Get.Time();
                    
                    self.Animation.Current.index = self.Animation.Current.index == self.Animation.Current.animation.numOfFrames - 1 ? 0  : self.Animation.Current.index + 1;
                    self.Animation.Current.frame = self.Animation.Current.animation.frames[self.Animation.Current.index];

                    self.graphic.html.style.backgroundPosition = -self.Animation.Current.frame.x + "px " + -self.Animation.Current.frame.y + "px";
                },
                Update: function(){   //Transition to Next Frame, if ready
                    
                },
                Start: function(){    //Auto Animate
                    
                },
                Pause: function(){    //Pause Animation 
                    
                },
                Reset: function(){    //Reset Animation to First Frame
                    self.Animation.lastUpdate = new Date().getDate();
                
                    self.Animation.Current.index = 0;
                    self.Animation.Current.frame = self.Animation.Current.animation.frames[0];
                },
                Load: function(oAnimation){     //Load New Animation into Sprite Object
                    self.Animation.list.put(oAnimation.name, oAnimation);
                },
                SetActive: function(sName){//Set Animation
                    //Remove Current Animation Class
                    if(self.Animation.Current.animation)
                        self.graphic.Remove.Class(self.Animation.Current.animation.sheet.GetClass() + " ");
                    
                    //Set new animation object
                    self.Animation.Current.animation = self.Animation.list.get(sName);
                    
                    //Add animation class to html
                    self.graphic.Append.Class(self.Animation.Current.animation.sheet.GetClass());
                    
                    //Update frame on graphic
                    self.Animation.SetFrame(0);
                },
                SetFrame: function(iFrameNumber){ //Set To Specific Frame in Current Animation
                    self.Animation.lastUpdate = new Date().getDate();
                
                    self.Animation.Current.index = iFrameNumber;
                    self.Animation.Current.frame = self.Animation.Current.animation.frames[iFrameNumber];

                    self.graphic.html.style.backgroundPosition = -self.Animation.Current.frame.x + "px " + -self.Animation.Current.frame.y + "px";
                }
                
            }
        }
    
        var CSpriteSheet = function(sName){
            var self = this;
            this.width = 0;
            this.height = 0;
            this.cellWidth = 0;
            this.cellHeight = 0;
            this.rows = 0;
            this.columns = 0;
            this.path = "";
            this.id = 0;
            this.name = sName;
            var className = ""
            this.BuildClass = function(sClassName){
                className = sClassName ? sClassName : self.name;
    
                var style = "position: absolute; ";
                style += "overflow: hidden;";
                style += "background: url('" + this.path + "'); ";
                style += "width: " + this.cellWidth +"px;";
                style += "height: " + this.cellHeight + "px;";
                
                pInstance.Create.CSSClass("."+className, style);
            }
            this.GetClass = function(){
                return className;
            }
        }
        
        var CAnimation = function(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet, speed){
            var self = this;
            this.name = sName;
            this.sheet = oSpriteSheet;
            this.numOfFrames = iNumOfFrames;
            this.StartFrame = null;
            this.StopFrame  = null;
            this.frames = [];
            this.speed =  speed ? speed : 0;
            //Set Stop Column, Row and index
            
            //Populate frames array
            
             {
                var _frame = 0;
                
                //need to have initial starting point, will be reset to 0 afterwards
                var _iStartColumn = iStartColumn;
                
                // runs through rows (y) and columns (x) and adds them to the frames array
                for(var y = iStartRow; y < oSpriteSheet.rows && _frame < iNumOfFrames; y++){
                    for (var x = _iStartColumn; x < oSpriteSheet.columns && _frame < iNumOfFrames; x++,_frame++){
                        var fr = new CAnimationFrame(x*oSpriteSheet.cellWidth,y*oSpriteSheet.cellHeight,oSpriteSheet.cellWidth,oSpriteSheet.cellHeight);
                        fr.frame = _frame;
                        fr.row = y;
                        fr.column = x;
                        self.frames.push(fr);
                    }
                    // resetting starting column (x) to 0, so it goes through all following columns
                    _iStartColumn = 0;

                }
            }
        }
    
        var CAnimationFrame = function(iX, iY, iWidth, iHeight){
            this.x = iX;
            this.y = iY;
            this.width = iWidth;
            this.height = iHeight;
            this.frame = 0;         //Sprite Sheet Frame Number
            this.row = 0;
            this.column = 0;
        }
        
        //Base Classes
        var CHTMLElement = function(tag){
                this.html = null;
                this.classes = "";
                var self = this;

                this.Append ={
                    Class: function(sClass){
                        self.classes += sClass + " ";
                        self.html.className = self.classes;
                        return self.classes;
                    },
                    Child: function(childHTML){
                        self.html.appendChild(childHTML);
                    },
                    ToID: function(parentID){
                        document.getElementById(parentID).appendChild(self.html);
                    },
                    ToHTML: function(parentHTML){
                        parentHTML.appendChild(self.html);
                    }
                }
                
                this.Remove = {
                    Class: function(sClass){
                        
                        self.classes = self.classes.replace(sClass + " ", "");
                        self.html.className = self.classes;
                        return self.classes;
                    },
                    Child: function(id){
                        self.html.removeChild(document.getElementById(id));
                    },
                    //removes all classes from class list
                    AllClasses: function(){
                        self.classes = "";
                        self.html.className = "";
                    },
                    AllChildren: function(){
                    
                        if ( self.html.hasChildNodes() )
                        {
                            while ( self.html.childNodes.length >= 1 )
                            {
                                self.html.removeChild( this.html.firstChild );       
                            } 
                        }
                    }
                    
                }
              
                this.SetHTML = function(oHTML){
                    self.html = oHTML;//document.getElementById(id);
                    //self.html = _html;
                    self.classes = this.html.className;
                }
                this.SetSelf = function(obj){
                    self = obj;
                }
                
                //Creates HTML element if tag exist        
                this.html =  tag ? document.createElement(tag) : null;
        
                return this;
            }
            
            
        //Map Object
        var CMap = function(linkEntries){
            this.current = undefined;
            this.size = 0;
            this.isLinked = true;
        
            if(linkEntries === false)
            {
                    this.disableLinking();
            }
                    
            this.from = function(obj, foreignKeys, linkEntries)
            {
                var map = new Map(linkEntries);
        
                for(var prop in obj) {
                        if(foreignKeys || obj.hasOwnProperty(prop))
                                map.put(prop, obj[prop]);
                }
        
                return map;
            }
            
            this.noop = function()
            {
                    return this;
            }
            
            this.illegal = function()
            {
                    throw new Error('can\'t do this with unlinked maps');
            }
            
            this.reverseIndexTableFrom = function(array, linkEntries)
            {
                var map = new Map(linkEntries);
        
                for(var i = 0, len = array.length; i < len; ++i) {
                        var	entry = array[i],
                                list = map.get(entry);
        
                        if(list) list.push(i);
                        else map.put(entry, [i]);
                }
        
                return map;
            }
        
            this.cross = function(map1, map2, func, thisArg)
            {
                var linkedMap, otherMap;
            
                if(map1.isLinked) {
                        linkedMap = map1;
                        otherMap = map2;
                }
                else if(map2.isLinked) {
                        linkedMap = map2;
                        otherMap = map1;
                }
                else Map.illegal();
            
                for(var i = linkedMap.size; i--; linkedMap.next()) {
                        var key = linkedMap.key();
                        if(otherMap.contains(key))
                                func.call(thisArg, key, map1.get(key), map2.get(key));
                }
            
                return thisArg;
            }
        
            this.uniqueArray = function(array)
            {
                    var map = new Map;
            
                    for(var i = 0, len = array.length; i < len; ++i)
                            map.put(array[i]);
            
                    return map.listKeys();
            }                                    
        };
        
        CMap.prototype.disableLinking = function(){
            this.isLinked = false;
            this.link = Map.noop;
            this.unlink = Map.noop;
            this.disableLinking = Map.noop;
            this.next = Map.illegal;
            this.key = Map.illegal;
            this.value = Map.illegal;
            this.removeAll = Map.illegal;
            this.each = Map.illegal;
            this.flip = Map.illegal;
            this.drop = Map.illegal;
            this.listKeys = Map.illegal;
            this.listValues = Map.illegal;
        
            return this;
        };
        
        CMap.prototype.hash = function(value){
            return value instanceof Object ? (value.__hash ||
                    (value.__hash = 'object ' + ++arguments.callee.current)) :
                    (typeof value) + ' ' + String(value);
        };
        
        CMap.prototype.hash.current = 0;            
        CMap.prototype.link = function(entry){
                if(this.size === 0) {
                        entry.prev = entry;
                        entry.next = entry;
                        this.current = entry;
                }
                else {
                        entry.prev = this.current.prev;
                        entry.prev.next = entry;
                        entry.next = this.current;
                        this.current.prev = entry;
                }
        };
        
        CMap.prototype.unlink = function(entry) {
                if(this.size === 0)
                        this.current = undefined;
                else {
                        entry.prev.next = entry.next;
                        entry.next.prev = entry.prev;
                        if(entry === this.current)
                                this.current = entry.next;
                }
        };
        
        CMap.prototype.get = function(key) {
                var entry = this[this.hash(key)];
                return typeof entry === 'undefined' ? undefined : entry.value;
        };
        
        CMap.prototype.put = function(key, value) {
                var hash = this.hash(key);
        
                if(this.hasOwnProperty(hash))
                        this[hash].value = value;
                else {
                        var entry = { key : key, value : value };
                        this[hash] = entry;
        
                        this.link(entry);
                        ++this.size;
                }
        
                return this;
        };
        
        CMap.prototype.remove = function(key) {
                var hash = this.hash(key);
        
                if(this.hasOwnProperty(hash)) {
                        --this.size;
                        this.unlink(this[hash]);
        
                        delete this[hash];
                }
        
                return this;
        };
        
        CMap.prototype.removeAll = function() {
                while(this.size)
                        this.remove(this.key());
        
                return this;
        };
        
        CMap.prototype.contains = function(key) {
                return this.hasOwnProperty(this.hash(key));
        };
       
        CMap.prototype.isUndefined = function(key) {
                var hash = this.hash(key);
                return this.hasOwnProperty(hash) ?
                        typeof this[hash] === 'undefined' : false;
        };
        
        CMap.prototype.next = function() {
                this.current = this.current.next;
        };
        
        CMap.prototype.key = function() {
                return this.current.key;
        };
        
        CMap.prototype.value = function() {
                return this.current.value;
        };
       
        CMap.prototype.each = function(func, thisArg) {
                if(typeof thisArg === 'undefined')
                        thisArg = this;
        
                for(var i = this.size; i--; this.next()) {
                        var n = func.call(thisArg, this.key(), this.value(), i > 0);
                        if(typeof n === 'number')
                                i += n; // allows to add/remove entries in func
                }
        
                return this;
        };
        
        CMap.prototype.flip = function(linkEntries) {
                var map = new Map(linkEntries);
        
                for(var i = this.size; i--; this.next()) {
                        var	value = this.value(),
                                list = map.get(value);
        
                        if(list) list.push(this.key());
                        else map.put(value, [this.key()]);
                }
        
                return map;
        };
        
        CMap.prototype.drop = function(func, thisArg) {
                if(typeof thisArg === 'undefined')
                        thisArg = this;
        
                for(var i = this.size; i--; ) {
                        if(func.call(thisArg, this.key(), this.value())) {
                                this.remove(this.key());
                                --i;
                        }
                        else this.next();
                }
        
                return this;
        };
        
        CMap.prototype.listValues = function() {
                var list = [];
        
                for(var i = this.size; i--; this.next())
                        list.push(this.value());
        
                return list;
        }
        
        CMap.prototype.listKeys = function() {
                var list = [];
        
                for(var i = this.size; i--; this.next())
                        list.push(this.key());
        
                return list;
        }
        
        CMap.prototype.toString = function() {
                var string = '[object Map';
        
                function addEntry(key, value, hasNext) {
                        string += '    { ' + this.hash(key) + ' : ' + value + ' }' +
                                (hasNext ? ',' : '') + '\n';
                }
        
                if(this.isLinked && this.size) {
                        string += '\n';
                        this.each(addEntry);
                }
        
                string += ']';
                return string;
        };
        
        
        
        //  Finite State Machine
        var CFiniteStateMachine = function(cEntity){
            var owner = cEntity;
            var _CurrentState = new CState();
            var self = this;
            var _Enter = function(oMessage){
                _CurrentState.Enter(owner, oMessage);
            }
            
            var _Exit = function(oMessage){
                _CurrentState.Exit(owner, oMessage);
            }
            
            var _Execute = function(oMessage){
                _CurrentState.Execute(owner, oMessage);
            }
            
            //Control Methods
            this.Transition = function(sName, oMessage){
                //Call Exit for Current State
                _Exit(oMessage);
                
                //Get New State to Transition Too
                _CurrentState = _States.get(sName);
                
                //Call New States Setup and Exectue Methods
                _Enter(oMessage);
                _Execute(oMessage);
            }
            this.SetState = function(sName){
                _CurrentState = _States.get(sName);
            }
            this.Update = function(oMessage){
                _Execute(oMessage);
            }
            this.GetState = function(){
                return _CurrentState.GetName();
            }
        }
        
        // States for FSM
        var CState = function(sName){
            var _name = sName;
            var self = this;
            
            this.Enter = function(cEntity, oMessage){
                //Override this Method
            }
            
            this.Exit = function(cEntity, oMessage){
                //Override this Method
            }
            
            this.Execute = function(cEntity, oMessage){
                //Override this Method
            }
            
            this.GetName = function(){
                return _name;
            }

        }
        //Holds all states created
        // key: name, value: CState
        var _States = new CMap();
        
        
        //
        //  INPUT
        //
        var _eventID = 0;
        
        var _InputEvents = {
            Mouse: {
                Down: function(e){
                    _Mouse.lastX = e.clientX;
                    _Mouse.lastY = e.clientY;
                    _Mouse.pressed = true;
                    
                    var callbackArray = _MouseEvents.get("DOWN");
                    if(callbackArray){
                        for(var i = callbackArray.length - 1; i >= 0 ; i--){
                            if(callbackArray[i].scope){
                                callbackArray[i].func.apply(callbackArray.scope, [e]);
                            }else{
                                callbackArray[i].func(e);
                            }
                        }
                    }
                    
                },
                Up: function(e){
                    _Mouse.pressed = false;
                    
                    var callbackArray = _MouseEvents.get("UP");
                    if(callbackArray){
                        for(var i = callbackArray.length - 1; i >= 0 ; i--){
                            if(callbackArray[i].scope){
                                callbackArray[i].func.apply(callbackArray.scope, [e]);
                            }else{
                                callbackArray[i].func(e);
                            }
                        }
                    }
                },
                Move: function(e){
                    if (!_Mouse.pressed) {
                        return;
                    }
                    
                    var callbackArray = _MouseEvents.get("MOVE");
                    if(callbackArray){
                        for(var i = callbackArray.length - 1; i >= 0 ; i--){
                            if(callbackArray[i].scope){
                                callbackArray[i].func.apply(callbackArray.scope, [e]);
                            }else{
                                callbackArray[i].func(e);
                            }
                        }
                    }
                
                    _Mouse.lastX = e.clientX;
                    _Mouse.lastY = e.clientY; 
                }
            },
            Touch: {
                Start: function(e){
                    e.preventDefault();
                    _Touch.lastX = e.touches[0].pageX;
                    _Touch.lastY = e.touches[0].pageY;
                    _Touch.startX = e.touches[0].pageX;
                    _Touch.startY = e.touches[0].pageY;
                    _Touch.pressed = true;
                    
                    var callbackArray = _TouchEvents.get("START");
                    if(callbackArray){
                        for(var i = callbackArray.length - 1; i >= 0 ; i--){
                            if(callbackArray[i].scope){
                                callbackArray[i].func.apply(callbackArray.scope, [e]);
                            }else{
                                callbackArray[i].func(e);
                            }
                        }
                    }
                    
                },
                End: function(e){
                    e.preventDefault();
                    _Touch.pressed = false;
                    _Touch.startX = 0;
                    _Touch.startY = 0;
                    
                    var callbackArray = _TouchEvents.get("END");
                    if(callbackArray){
                        for(var i = callbackArray.length - 1; i >= 0 ; i--){
                            if(callbackArray[i].scope){
                                callbackArray[i].func.apply(callbackArray.scope, [e]);
                            }else{
                                callbackArray[i].func(e);
                            }
                        }
                    }
                },
                Move: function(e){
                    e.preventDefault();
                    if (!_Touch.pressed) {
                        return;
                    }
                    
                    var callbackArray = _TouchEvents.get("MOVE");
                    if(callbackArray){
                        for(var i = callbackArray.length - 1; i >= 0 ; i--){
                            if(callbackArray[i].scope){
                                callbackArray[i].func.apply(callbackArray.scope, [e]);
                            }else{
                                callbackArray[i].func(e);
                            }
                        }
                    }
                
                    _Touch.lastX = e.touches[0].pageX;
                    _Touch.lastY = e.touches[0].pageY; 
                }
            },
            Keyboard:{
                Keydown: function(e){
                    
                    var callbackArray = e.keyCode > 47 && e.keyCode < 91 ? _KeyDownEvents.get(String.fromCharCode(e.keyCode)) : _KeyboardControlKeyEvents.get(e.keyCode);
                                      
                    if(callbackArray){
                        for(var i = callbackArray.length - 1; i >= 0 ; i--){
                            if(callbackArray[i].scope){
                                callbackArray[i].func.apply(callbackArray.scope, [e]);
                            }else{
                                callbackArray[i].func(e);
                            }
                        }
                    }
                },
                Keyup: function(e){
                    var callbackArray = e.keyCode > 47 && e.keyCode < 91 ? _KeyUpEvents.get(String.fromCharCode(e.keyCode)) : _KeyboardControlKeyEvents.get(e.keyCode);
                                      
                    if(callbackArray){
                        for(var i = callbackArray.length - 1; i >= 0 ; i--){
                            if(callbackArray[i].scope){
                                callbackArray[i].func.apply(callbackArray.scope, [e]);
                            }else{
                                callbackArray[i].func(e);
                            }
                        }
                    }
                }
            }
        }
        
        var CCallback = function(iID, fFunction, oScope){
            this.func = fFunction;
            this.scope =  oScope == undefined ? false : oScope;
            this.id = iID;
        }
        
        
        //Holds state for Touch Events
        var _Touch = {
            startX: 0,
            startY: 0,
            lastX: 0,
            lastY: 0,
            pressed: false
        }
        
        //      key: mouseEventType, value: Array[CCallbacks]
        var _TouchEvents = new CMap();
        _TouchEvents.put("START", []);
        _TouchEvents.put("END", []);
        _TouchEvents.put("MOVE", []);
        document.addEventListener("touchstart", _InputEvents.Touch.Start, false);
        document.addEventListener("touchend", _InputEvents.Touch.End, false);
        document.addEventListener("touchmove", _InputEvents.Touch.Move, false);
        
        //Holds state of mouse for all registered states
        var _Mouse = {
            lastX: 0,
            lastY: 0,
            pressed: false
        }
        
        //      key: mouseEventType, value: Array[CCallbacks]
        var _MouseEvents = new CMap();
        _MouseEvents.put("DOWN", []);
        _MouseEvents.put("UP", []);
        _MouseEvents.put("MOVE", []);
        document.addEventListener("mousedown", _InputEvents.Mouse.Down, false);
        document.addEventListener("mouseup", _InputEvents.Mouse.Up, false);
        document.addEventListener("mousemove", _InputEvents.Mouse.Move, false);
        
        //Holds states/events of all keys
        //      key: keyCode, value: true/false
        var _KeyboardStates = new CMap();
        
        //      key: keyCode, value: Array[CCallbacks];
        var _KeyDownEvents = new CMap();
        var _KeyUpEvents = new CMap();
        var _KeyboardControlKeyEvents = new CMap();
        window.addEventListener("keydown", _InputEvents.Keyboard.Keydown, false);
        window.addEventListener("keyup", _InputEvents.Keyboard.Keyup, false);
        
        var Animations = new CMap();
        var SpriteSheets = new CMap();
        var Sprites = new CMap();
        
        //
        //  Sound API
        //
        var sounds = new CMap();
        
        var CSound =  function(sKey){
            var self = this;
            this.key = sKey;
            this.path = "";
            this.loop = false;
            this.autoplay = false;
            this.type = "";
            this.duration = "";
            this.volume = "";
            this.audio =  new CHTMLElement("audio");
        }
        CSound.prototype.Play = function(){
            this.audio.html.play();
        }
        CSound.prototype.Pause = function(){
            this.audio.html.pause();
        }
        
        //PUBLIC OBJECTS/METHODS
        return{
            Get: {
                Sprite: function(sName){
                    return Sprites.get(sName);
                },
                SpriteSheet: function(sName){
                    return SpriteSheets.get(sName)
                },
                Animation: function(sName){
                    return Animations.get(sName);
                },
                Time: function(){
                    return Date.now();
                },
                State: function(sName){
                    return _States.get(sName);  
                },
                Name: function(){
                    return NAME;
                },
                Version: function(){
                    return VERSION;
                },
                Path: function(){
                    return PATH;
                }
            },
            Remove: {
                Sprite: function(sName){
                    return Sprites.drop(sName);
                },
                SpriteSheet: function(sName){
                    return SpriteSheets.drop(sName)
                },
                Animation: function(sName){
                    return Animations.drop(sName);
                }
            },
            
            Create: {
                HTML: function(tag){
                    return new CHTMLElement(tag);
                },
                Map: function(){
                    return new CMap();
                },
                FSM: function(cEntity){
                    return new CFiniteStateMachine(cEntity);
                    
                },
                State: function(sName){
                    var state = new CState(sName);
                    _States.put(sName, state);
                    return state;
                },
                Sprite: function(sName){
                    var sprite = new CSprite(sName);
                    Sprites.put(sName, sprite);
                    
                    return  sprite;
                },
                SpriteSheet: function(sName){
                    var sheet = new CSpriteSheet(sName);
                    SpriteSheets.put(sName, sheet);
                    
                    return sheet;   
                },
                Animation: function(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet, speed){
                    var ani = new CAnimation(sName,iNumOfFrames,iStartRow, iStartColumn,oSpriteSheet, speed);
                    Animations.put(sName, ani);
                    
                    return ani;
                },
                AnimationFrame: function(iX, iY, iWidth, iHeight){
                    return new CAnimationFrame(iX, iY, iWidth, iHeight);
                },
                CSSClass: function(sClass, sStyle) {
                    if (!document.styleSheets) {
                        return;
                    }
                
                    if (document.getElementsByTagName("head").length == 0) {
                        return;
                    }
                
                    var stylesheet;
                    var mediaType;
                    if (document.styleSheets.length > 0) {
                        for (i = 0; i < document.styleSheets.length; i++) {
                            if (document.styleSheets[i].disabled) {
                                continue;
                            }
                            var media = document.styleSheets[i].media;
                            mediaType = typeof media;
                
                            if (mediaType == "string") {
                                if (media == "" || (media.indexOf("screen") != -1)) {
                                    styleSheet = document.styleSheets[i];
                                }
                            } else if (mediaType == "object") {
                                if (media.mediaText == "" || (media.mediaText.indexOf("screen") != -1)) {
                                    styleSheet = document.styleSheets[i];
                                }
                            }
                
                            if (typeof styleSheet != "undefined") {
                                break;
                            }
                        }
                    }
                
                    if (typeof styleSheet == "undefined") {
                        var styleSheetElement = document.createElement("style");
                        styleSheetElement.type = "text/css";
                
                        document.getElementsByTagName("head")[0].appendChild(styleSheetElement);
                
                        for (i = 0; i < document.styleSheets.length; i++) {
                            if (document.styleSheets[i].disabled) {
                                continue;
                            }
                            styleSheet = document.styleSheets[i];
                        }
                
                        var media = styleSheet.media;
                        mediaType = typeof media;
                    }
                
                    if (mediaType == "string") {
                        for (i = 0; i < styleSheet.rules.length; i++) {
                            if (styleSheet.rules[i].selectorText.toLowerCase() == sClass.toLowerCase()) {
                                styleSheet.rules[i].style.cssText = sStyle;
                                return;
                            }
                        }
                
                        styleSheet.addRule(sClass, sStyle);
                    }
                    else if (mediaType == "object") {
                        for (i = 0; i < styleSheet.cssRules.length; i++) {
                            if (styleSheet.cssRules[i].selectorText.toLowerCase() == sClass.toLowerCase()) {
                                styleSheet.cssRules[i].style.cssText = sStyle;
                                return;
                            }
                        }
                
                        styleSheet.insertRule(sClass + "{" + sStyle + "}", 0);
                    }
                }
                
            },
            Input: {
                currentTime: 0,
                previousTime: 0,
                Get: {
                    State: {
                        Mouse: function(){
                            return _Mouse;
                        },
                        Touch: function(){
                            return _Touch;
                        },
                        Keyboard: function(keyCode){
                            return _KeyboardStates.get(keyCode);
                        }
                    }
                },
                Register: {
                    Mouse: {
                        Event: {
                            Down: function(fFunction, oScope){
                                var e = new CCallback(_eventID++, fFunction, oScope);
                                _MouseEvents.get("DOWN").push(e);
                                return e;
                            },
                            Up: function(fFunction, oScope){
                                var e = new CCallback(_eventID++, fFunction, oScope);
                                _MouseEvents.get("UP").push(e);
                                return e;
                            },
                            Move: function(fFunction, oScope){
                                var e = new CCallback(_eventID++, fFunction, oScope);
                                _MouseEvents.get("MOVE").push(e);
                                return e;
                            }
                        },
                        State: {
                            
                        }
                    },
                    Touch: {
                        Event: {
                            Start: function(fFunction, oScope){
                                var e = new CCallback(_eventID++, fFunction, oScope);
                                _TouchEvents.get("START").push(e);
                                return e;
                            },
                            End: function(fFunction, oScope){
                                var e = new CCallback(_eventID++, fFunction, oScope);
                                _TouchEvents.get("END").push(e);
                                return e;
                            },
                            Move: function(fFunction, oScope){
                                var e = new CCallback(_eventID++, fFunction, oScope);
                                _TouchEvents.get("MOVE").push(e);
                                return e;
                            }
                        },
                        State: {
                            
                        }
                    },
                    Keyboard: {
                        Event: {
                            Keydown: function(Key, fFunction, oScope){
                                var e = new CCallback(_eventID++, fFunction, oScope);
                                var eventArray;
                                
                                if(isNaN(Key)){
                                    eventArray = _KeyboardControlKeyEvents.get(Key);
                                }
                                else{
                                    eventArray = _KeyboardControlKeyEvents.get(Key);
                                }
                                
                                
                                if(!eventArray){
                                    eventArray = [];
                                    if(isNaN(Key)){ _KeyDownEvents.put(Key, eventArray);}
                                    else{_KeyboardControlKeyEvents.put(Key, eventArray);}
                                    
                                }
                                eventArray.push(e);
                                return e;
                            },
                            Keyup: function(Key, fFunction, oScope){
                                var e = new CCallback(_eventID++, fFunction, oScope);
                                var eventArray;
                                
                                if(isNaN(Key)){
                                    eventArray = _KeyboardControlKeyEvents.get(Key);
                                }
                                else{
                                    eventArray = _KeyboardControlKeyEvents.get(Key);
                                }
                                
                                
                                if(!eventArray){
                                    eventArray = [];
                                    if(isNaN(Key)){ _KeyUpEvents.put(Key, eventArray);}
                                    else{_KeyboardControlKeyEvents.put(Key, eventArray);}
                                    
                                }
                                eventArray.push(e);
                                return e;
                                
                            }
                        },
                        State: {
                            
                        }
                    }
                }
            },
            Audio: {
                Add: function(sKey, sPath, bLoop, bAutoplay, sType){       
                    var snd = new CSound(sKey);
                    snd.loop = bLoop;
                    snd.autoplay = bAutoplay;
                    
                    if(sType){
                        snd.type = sType;
                        snd.path = sPath;
                        var source = document.createElement('source');
                        if(sType.toUpperCase() == "MP3"){
                            
                            if (snd.audio.html.canPlayType('audio/mpeg;')) {
                                source.type= 'audio/mpeg';
                                source.src= sPath;
                                snd.audio.html.setAttribute('preload', 'auto');
                                
                                if(bLoop){snd.audio.html.setAttribute('loop', bLoop);}
                                if(bAutoplay){snd.audio.html.setAttribute('autoplay', bAutoplay);}
                                
                                snd.audio.html.appendChild(source);
                                
                                sounds.put(sKey, snd);
                            }
                            else{
                                //os.windows.Create.ErrorWindow(" Audio", "Audio File: " +  sKey + " not loaded <br/>Audio File Type: " + sType + " is not supported by browser")
                            }
                            
                        }
                        else if(sType.toUpperCase == "OGG"){
                            
                            if(snd.audio.html.canPlayType('audio/ogg;')) {
                                source.type= 'audio/ogg';
                                source.src= sPath;
                                snd.audio.html.setAttribute('preload', 'auto');
                                
                                if(loop){snd.audio.html.setAttribute('loop', bLoop);}
                                if(autoplay){snd.audio.html.setAttribute('autoplay', bAutoplay);}
                                
                                snd.audio.html.appendChild(source);
                                
                                sounds.put(sKey, snd);
                            }
                            else{
                                //os.windows.Create.ErrorWindow(" Audio", "Audio File: " +  sKey + " not loaded <br/>Audio File Type: " + sType + " is not supported by browser")
                            }
                        }
                    }
                    else{
                        snd.type = sType;
                        snd.path = sPath;
                        var source = document.createElement('source');
                        
                        if (snd.audio.html.canPlayType('audio/mpeg;')) {
                            source.type= 'audio/mpeg';
                            source.src= sPath + ".mp3";
                            snd.audio.html.setAttribute('preload', 'auto');
                            
                            if(bLoop){snd.audio.html.setAttribute('loop', bLoop);}
                            if(bAutoplay){snd.audio.html.setAttribute('autoplay', bAutoplay);}
                            
                            snd.audio.html.appendChild(source);
                            
                            sounds.put(sKey, snd);
                        }
                        else if(snd.audio.html.canPlayType('audio/ogg;')) {
                            source.type= 'audio/ogg';
                            source.src= sPath + ".ogg";
                            snd.audio.html.setAttribute('preload', 'auto');
                            
                            if(bLoop){snd.audio.html.setAttribute('loop', bLoop);}
                            if(bAutoplay){snd.audio.html.setAttribute('autoplay', bAutoplay);}
                            
                            snd.audio.html.appendChild(source);
                            
                            sounds.put(sKey, snd);
                        }
                        
                    }
                    
                    snd.duration = snd.audio.html.duration;
                    snd.volume = snd.audio.html.volume;
                    
                    return snd;
                    
                },
                Get: {
                    Volume: function(sKey){
                        return sounds.get(sKey).volume;
                    },
                    Sound: function(sKey){
                        return sounds.get(sKey);
                    },
                    Duration: function(sKey){
                        return sounds.get(sKey).audio.html().duration;
                    }
                    
                },
                Set: {
                    Volume: function(sKey, vol){
                        var snd = sounds.get(sKey);
                        snd.audio.html.volume = vol;
                        snd.volume = vol;
                    },
                    CurrentTime: function(sKey, time){
                        var snd = sounds.get(sKey);
                        snd.audio.html.currentTime = time;
                    }
                },
                Play: function(sKey){
                    sounds.get(sKey).audio.html().play();
                },
                Pause: function(sKey){
                    sounds.get(sKey).audio.html().pause();
                }
            },
            Modules:{
                
            },
            CHTMLElement:  function(tag){
                this.html = null;
                this.classes = "";
                var self = this;
                
                        
                this.Append ={
                    Class: function(sClass){
                        self.classes += sClass + " ";
                        self.html.className = self.classes;
                        return self.classes;
                    },
                    Child: function(childHTML){
                        self.html.appendChild(childHTML);
                    },
                    ToID: function(parentID){
                        document.getElementById(parentID).appendChild(self.html);
                    },
                    ToHTML: function(parentHTML){
                        parentHTML.appendChild(self.html);
                    }
                }
                
                this.Remove = {
                    Class: function(sClass){
                        
                        self.classes = self.classes.replace(sClass, "");
                        
                        return self.classes;
                    },
                    Child: function(id){
                        self.html.removeChild(document.getElementById(id));
                    },
                    //removes all classes from class list
                    AllClasses: function(){
                        self.classes = "";
                        self.html.className = "";
                    },
                    AllChildren: function(){
                    
                        if ( self.html.hasChildNodes() )
                        {
                            while ( self.html.childNodes.length >= 1 )
                            {
                                self.html.removeChild( this.html.firstChild );       
                            } 
                        }
                    }
                    
                }
              
                this.SetHTML = function(oHTML){
                    self.html = oHTML;//document.getElementById(id);
                    //self.html = _html;
                    self.classes = this.html.className;
                }
                this.SetSelf = function(obj){
                    self = obj;
                }
                
                //Creates HTML element if tag exist        
                this.html =  tag ? document.createElement(tag) : null;
        
                return this;
            }
                
        }
    }
    return {
        //Instantiates Objects and Returns
        //  Insures a single object
        Instance: function()
        {
            if(!pInstance)
            {
                //Instantiate if pInstance does not exist
                pInstance = constructor();
            }
            
            return pInstance;
        }
    }
})();